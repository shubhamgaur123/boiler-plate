import React from 'react';
import { connect } from 'react-redux';

class TestReducer extends React.Component {
    componentDidMount() {

        this.props.nameChange();
    }
    render() {
        return (
            <>
                {'{'}
                Name: {this.props.name}
                {'}'}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.reducerName.name
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        nameChange: () => dispatch({ type: 'CHANGE_NAME' })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestReducer);