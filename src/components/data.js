export default {
    "name": 'Top Parent',
    "children": [
        {
            "name": "analytics",
            "size": 100
        },
        {
            "name": "animate",
            "size": 100
        },
        {
            "name": "data",
            "size": 100

        },
        {
            "name": "display",
            "size": 100

        },
        {
            "name": "flex",
            "size": 100

        },
        {
            "name": "physics",
            "size": 100

        },
        {
            "name": "query",
            "size": 100

        },
        {
            "name": "scale",
            "size": 100
        },
        {
            "name": "util",
            "size": 100
        },
        {
            "name": "vis",
            "size": 100
        },
        {
            "name": "controls",
            "size": 100
        },
        {
            "name": "data",
            "size": 100
        },
        {
            "name": "events",
            "size": 100
        },
        {
            "name": "legend",
            "size": 100
        },
        {
            "name": "operator",
            "size": 100
        }
    ]
}