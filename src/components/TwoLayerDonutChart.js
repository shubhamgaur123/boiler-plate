import * as React from "react";
import * as d3 from "d3";
import { connect } from "react-redux";

let dataInner = [
	{
		production: 0.362183333,
		consumption: 0.46625,
		stage: "12:00AM"
	},
	{
		production: 0.36335,
		consumption: 0.47275,
		stage: "12:01AM"
	},
	{
		production: 0.35345,
		consumption: 0.4708,
		stage: "12:02AM"
	},
	{
		production: 0.335766667,
		consumption: 0.4638,
		stage: "12:03AM"
	},
	{
		production: 0.32065,
		consumption: 0.47015,
		stage: "12:04AM"
	},
	{
		production: 0.311183333,
		consumption: 0.468483333,
		stage: "12:05AM"
	},
	{
		production: 0.303933333,
		consumption: 0.462583333,
		stage: "12:06AM"
	},
	{
		production: 0.3002,
		consumption: 0.50525,
		stage: "12:07AM"
	},
	{
		production: 0.306683333,
		consumption: 0.514766667,
		stage: "12:08AM"
	},
	{
		production: 0.297633333,
		consumption: 0.516516667,
		stage: "12:09AM"
	},
	{
		production: 0.292766667,
		consumption: 3.10675,
		stage: "12:10AM"
	},
	{
		production: 0.28945,
		consumption: 4.05565,
		stage: "12:11AM"
	},
	{
		production: 0.287233333,
		consumption: 3.931083333,
		stage: "12:12AM"
	},
	{
		production: 0.28045,
		consumption: 3.853666667,
		stage: "12:13AM"
	},
	{
		production: 0.2714,
		consumption: 3.801266667,
		stage: "12:14AM"
	},
	{
		production: 0.260566667,
		consumption: 3.763766667,
		stage: "12:15AM"
	},
	{
		production: 0.25025,
		consumption: 3.770816667,
		stage: "12:16AM"
	},
	{
		production: 0.237033333,
		consumption: 2.518066667,
		stage: "12:17AM"
	},
	{
		production: 0.224366667,
		consumption: 0.831483333,
		stage: "12:18AM"
	},
	{
		production: 0.217866667,
		consumption: 0.387766667,
		stage: "12:19AM"
	},
	{
		production: 0.209683333,
		consumption: 0.38755,
		stage: "12:20AM"
	},
	{
		production: 0.2056,
		consumption: 0.380116667,
		stage: "12:21AM"
	},
	{
		production: 0.188616667,
		consumption: 0.390516667,
		stage: "12:22AM"
	},
	{
		production: 0.161733333,
		consumption: 0.353566667,
		stage: "12:23AM"
	},
	{
		production: 0.14055,
		consumption: 0.33875,
		stage: "12:24AM"
	},
	{
		production: 0.1275,
		consumption: 0.336033333,
		stage: "12:25AM"
	},
	{
		production: 0.1136,
		consumption: 0.349716667,
		stage: "12:26AM"
	},
	{
		production: 0.10535,
		consumption: 0.393533333,
		stage: "12:27AM"
	},
	{
		production: 0.0904,
		consumption: 0.470133333,
		stage: "12:28AM"
	},
	{
		production: 0.081983333,
		consumption: 0.445816667,
		stage: "12:29AM"
	},
	{
		production: 0.073066667,
		consumption: 0.4559,
		stage: "12:30AM"
	},
	{
		production: 0.064033333,
		consumption: 0.491666667,
		stage: "12:31AM"
	},
	{
		production: 0.057716667,
		consumption: 0.495083333,
		stage: "12:32AM"
	},
	{
		production: 0.050133333,
		consumption: 0.5026,
		stage: "12:33AM"
	},
	{
		production: 0.039583333,
		consumption: 0.504333333,
		stage: "12:34AM"
	},
	{
		production: 0.031116667,
		consumption: 0.467233333,
		stage: "12:35AM"
	},
	{
		production: 0.0267,
		consumption: 0.454933333,
		stage: "12:36AM"
	},
	{
		production: 0.022416667,
		consumption: 0.447233333,
		stage: "12:37AM"
	},
	{
		production: 0.023516667,
		consumption: 1.68505,
		stage: "12:38AM"
	},
	{
		production: 0.0236,
		consumption: 3.758,
		stage: "12:39AM"
	},
	{
		production: 0.022166667,
		consumption: 3.846033333,
		stage: "12:40AM"
	},
	{
		production: 0.01815,
		consumption: 3.874833333,
		stage: "12:41AM"
	},
	{
		production: 0.014933333,
		consumption: 3.8673,
		stage: "12:42AM"
	},
	{
		production: 0.009566667,
		consumption: 2.625516667,
		stage: "12:43AM"
	},
	{
		production: 0.001283333,
		consumption: 1.004066667,
		stage: "12:44AM"
	},
	{
		production: -0.002716667,
		consumption: 0.550216667,
		stage: "12:45AM"
	},
	{
		production: -0.008383333,
		consumption: 0.556366667,
		stage: "12:46AM"
	},
	{
		production: -0.012183333,
		consumption: 0.5132,
		stage: "12:47AM"
	},
	{
		production: -0.00815,
		consumption: 0.516516667,
		stage: "12:48AM"
	},
	{
		production: -0.004216667,
		consumption: 0.49595,
		stage: "12:49AM"
	},
	{
		production: -0.0042,
		consumption: 0.503066667,
		stage: "12:50AM"
	},
	{
		production: -0.004216667,
		consumption: 0.49665,
		stage: "12:51AM"
	},
	{
		production: -0.004333333,
		consumption: 0.49195,
		stage: "12:52AM"
	},
	{
		production: -0.004316667,
		consumption: 0.5002,
		stage: "12:53AM"
	},
	{
		production: -0.004333333,
		consumption: 0.499933333,
		stage: "12:54AM"
	},
	{
		production: -0.004233333,
		consumption: 0.52755,
		stage: "12:55AM"
	},
	{
		production: -0.004216667,
		consumption: 0.545016667,
		stage: "12:56AM"
	},
	{
		production: -0.0042,
		consumption: 0.545133333,
		stage: "12:57AM"
	},
	{
		production: -0.0033,
		consumption: 0.5377,
		stage: "12:58AM"
	},
	{
		production: -0.002366667,
		consumption: 0.541366667,
		stage: "12:59AM"
	},
	{
		production: -0.002433333,
		consumption: 0.489416667,
		stage: "01:00AM"
	},
	{
		production: -0.002433333,
		consumption: 0.496116667,
		stage: "01:01AM"
	},
	{
		production: -0.002433333,
		consumption: 0.495516667,
		stage: "01:02AM"
	},
	{
		production: -0.00245,
		consumption: 0.4949,
		stage: "01:03AM"
	},
	{
		production: 0.001516667,
		consumption: 2.73835,
		stage: "01:04AM"
	},
	{
		production: 0.0032,
		consumption: 3.709616667,
		stage: "01:05AM"
	},
	{
		production: 0.0033,
		consumption: 3.754466667,
		stage: "01:06AM"
	},
	{
		production: 0.0033,
		consumption: 3.7425,
		stage: "01:07AM"
	},
	{
		production: -0.000533333,
		consumption: 1.622283333,
		stage: "01:08AM"
	},
	{
		production: -0.002066667,
		consumption: 0.721466667,
		stage: "01:09AM"
	},
	{
		production: -0.002566667,
		consumption: 0.434983333,
		stage: "01:10AM"
	},
	{
		production: -0.002533333,
		consumption: 0.4426,
		stage: "01:11AM"
	},
	{
		production: -0.002533333,
		consumption: 0.4422,
		stage: "01:12AM"
	},
	{
		production: -0.0026,
		consumption: 0.404233333,
		stage: "01:13AM"
	},
	{
		production: -0.0026,
		consumption: 0.393116667,
		stage: "01:14AM"
	},
	{
		production: -0.002616667,
		consumption: 0.384533333,
		stage: "01:15AM"
	},
	{
		production: -0.0026,
		consumption: 0.3912,
		stage: "01:16AM"
	},
	{
		production: -0.0026,
		consumption: 0.390783333,
		stage: "01:17AM"
	},
	{
		production: -0.002666667,
		consumption: 0.377433333,
		stage: "01:18AM"
	},
	{
		production: -0.002366667,
		consumption: 0.541016667,
		stage: "01:19AM"
	},
	{
		production: -0.002033333,
		consumption: 0.7215,
		stage: "01:20AM"
	},
	{
		production: -0.002333333,
		consumption: 0.5442,
		stage: "01:21AM"
	},
	{
		production: -0.00235,
		consumption: 0.5544,
		stage: "01:22AM"
	},
	{
		production: -0.002266667,
		consumption: 0.5612,
		stage: "01:23AM"
	},
	{
		production: -0.002266667,
		consumption: 0.560783333,
		stage: "01:24AM"
	},
	{
		production: -0.002333333,
		consumption: 0.549566667,
		stage: "01:25AM"
	},
	{
		production: -0.002366667,
		consumption: 0.510433333,
		stage: "01:26AM"
	},
	{
		production: -0.002316667,
		consumption: 0.5197,
		stage: "01:27AM"
	},
	{
		production: -0.00235,
		consumption: 0.519833333,
		stage: "01:28AM"
	},
	{
		production: -0.00235,
		consumption: 0.510066667,
		stage: "01:29AM"
	},
	{
		production: -0.0023,
		consumption: 0.536483333,
		stage: "01:30AM"
	},
	{
		production: -0.002366667,
		consumption: 0.505966667,
		stage: "01:31AM"
	},
	{
		production: -0.002366667,
		consumption: 0.515016667,
		stage: "01:32AM"
	},
	{
		production: -0.002283333,
		consumption: 0.516916667,
		stage: "01:33AM"
	},
	{
		production: -0.0022,
		consumption: 0.555516667,
		stage: "01:34AM"
	},
	{
		production: -0.0022,
		consumption: 0.555016667,
		stage: "01:35AM"
	},
	{
		production: -0.002233333,
		consumption: 0.54535,
		stage: "01:36AM"
	},
	{
		production: -0.0022,
		consumption: 0.5489,
		stage: "01:37AM"
	},
	{
		production: -0.002333333,
		consumption: 0.499816667,
		stage: "01:38AM"
	},
	{
		production: -0.002333333,
		consumption: 0.50875,
		stage: "01:39AM"
	},
	{
		production: -0.002333333,
		consumption: 0.509083333,
		stage: "01:40AM"
	},
	{
		production: -0.002366667,
		consumption: 0.500416667,
		stage: "01:41AM"
	},
	{
		production: -0.002333333,
		consumption: 0.50895,
		stage: "01:42AM"
	},
	{
		production: -0.002333333,
		consumption: 0.506133333,
		stage: "01:43AM"
	},
	{
		production: -0.002333333,
		consumption: 0.490933333,
		stage: "01:44AM"
	},
	{
		production: -0.00225,
		consumption: 0.535883333,
		stage: "01:45AM"
	},
	{
		production: -0.002233333,
		consumption: 0.5422,
		stage: "01:46AM"
	},
	{
		production: -0.002233333,
		consumption: 0.532816667,
		stage: "01:47AM"
	},
	{
		production: -0.002233333,
		consumption: 0.541233333,
		stage: "01:48AM"
	},
	{
		production: -0.00225,
		consumption: 0.526616667,
		stage: "01:49AM"
	},
	{
		production: -0.002283333,
		consumption: 0.493333333,
		stage: "01:50AM"
	},
	{
		production: -0.002483333,
		consumption: 0.394933333,
		stage: "01:51AM"
	},
	{
		production: -0.0025,
		consumption: 0.385466667,
		stage: "01:52AM"
	},
	{
		production: -0.002466667,
		consumption: 0.393933333,
		stage: "01:53AM"
	},
	{
		production: -0.0025,
		consumption: 0.441666667,
		stage: "01:54AM"
	},
	{
		production: -0.0025,
		consumption: 0.461333333,
		stage: "01:55AM"
	},
	{
		production: -0.002533333,
		consumption: 0.49315,
		stage: "01:56AM"
	},
	{
		production: -0.002433333,
		consumption: 0.596466667,
		stage: "01:57AM"
	},
	{
		production: -0.00235,
		consumption: 0.6328,
		stage: "01:58AM"
	},
	{
		production: -0.0024,
		consumption: 0.619983333,
		stage: "01:59AM"
	},
	{
		production: -0.002266667,
		consumption: 0.717183333,
		stage: "02:00AM"
	},
	{
		production: -0.002016667,
		consumption: 0.831466667,
		stage: "02:01AM"
	},
	{
		production: -0.002066667,
		consumption: 0.90215,
		stage: "02:02AM"
	},
	{
		production: -0.002033333,
		consumption: 0.8921,
		stage: "02:03AM"
	},
	{
		production: -0.002016667,
		consumption: 0.905633333,
		stage: "02:04AM"
	},
	{
		production: -0.002016667,
		consumption: 0.887783333,
		stage: "02:05AM"
	},
	{
		production: -0.002,
		consumption: 0.904833333,
		stage: "02:06AM"
	},
	{
		production: -0.002016667,
		consumption: 0.90465,
		stage: "02:07AM"
	},
	{
		production: -0.002033333,
		consumption: 0.8967,
		stage: "02:08AM"
	},
	{
		production: -0.002033333,
		consumption: 0.918883333,
		stage: "02:09AM"
	},
	{
		production: -0.002,
		consumption: 0.917516667,
		stage: "02:10AM"
	},
	{
		production: -0.00195,
		consumption: 0.939816667,
		stage: "02:11AM"
	},
	{
		production: -0.001933333,
		consumption: 0.946566667,
		stage: "02:12AM"
	},
	{
		production: -0.001866667,
		consumption: 0.9774,
		stage: "02:13AM"
	},
	{
		production: -0.001816667,
		consumption: 1.01085,
		stage: "02:14AM"
	},
	{
		production: -0.001866667,
		consumption: 0.99575,
		stage: "02:15AM"
	},
	{
		production: -0.00175,
		consumption: 1.0721,
		stage: "02:16AM"
	},
	{
		production: -0.001766667,
		consumption: 1.058,
		stage: "02:17AM"
	},
	{
		production: -0.00185,
		consumption: 1.031466667,
		stage: "02:18AM"
	},
	{
		production: -0.001783333,
		consumption: 1.0402,
		stage: "02:19AM"
	},
	{
		production: -0.001816667,
		consumption: 1.022333333,
		stage: "02:20AM"
	},
	{
		production: -0.0018,
		consumption: 1.028883333,
		stage: "02:21AM"
	},
	{
		production: -0.00135,
		consumption: 1.287883333,
		stage: "02:22AM"
	},
	{
		production: -0.000633333,
		consumption: 1.739416667,
		stage: "02:23AM"
	},
	{
		production: -0.000583333,
		consumption: 1.774733333,
		stage: "02:24AM"
	},
	{
		production: -0.000616667,
		consumption: 1.775983333,
		stage: "02:25AM"
	},
	{
		production: -0.000966667,
		consumption: 1.5407,
		stage: "02:26AM"
	},
	{
		production: -0.0013,
		consumption: 1.3033,
		stage: "02:27AM"
	},
	{
		production: -0.0007,
		consumption: 1.654833333,
		stage: "02:28AM"
	},
	{
		production: -0.000683333,
		consumption: 1.615483333,
		stage: "02:29AM"
	},
	{
		production: -0.0007,
		consumption: 1.615133333,
		stage: "02:30AM"
	},
	{
		production: -0.001083333,
		consumption: 1.378633333,
		stage: "02:31AM"
	},
	{
		production: -0.001433333,
		consumption: 1.148583333,
		stage: "02:32AM"
	},
	{
		production: -0.0007,
		consumption: 1.606716667,
		stage: "02:33AM"
	},
	{
		production: -0.000683333,
		consumption: 1.696683333,
		stage: "02:34AM"
	},
	{
		production: -0.000616667,
		consumption: 1.742583333,
		stage: "02:35AM"
	},
	{
		production: -0.001166667,
		consumption: 1.409266667,
		stage: "02:36AM"
	},
	{
		production: 0.001733333,
		consumption: 3.12805,
		stage: "02:37AM"
	},
	{
		production: 0.004233333,
		consumption: 4.713216667,
		stage: "02:38AM"
	},
	{
		production: 0.004216667,
		consumption: 4.713666667,
		stage: "02:39AM"
	},
	{
		production: 0.0042,
		consumption: 4.690116667,
		stage: "02:40AM"
	},
	{
		production: 0.000516667,
		consumption: 2.4512,
		stage: "02:41AM"
	},
	{
		production: -0.0014,
		consumption: 1.257216667,
		stage: "02:42AM"
	},
	{
		production: -0.001066667,
		consumption: 1.424816667,
		stage: "02:43AM"
	},
	{
		production: -0.001083333,
		consumption: 1.40375,
		stage: "02:44AM"
	},
	{
		production: -0.002333333,
		consumption: 0.723583333,
		stage: "02:45AM"
	},
	{
		production: -0.00235,
		consumption: 0.6153,
		stage: "02:46AM"
	},
	{
		production: -0.002333333,
		consumption: 0.544116667,
		stage: "02:47AM"
	},
	{
		production: -0.0025,
		consumption: 0.4248,
		stage: "02:48AM"
	},
	{
		production: -0.0025,
		consumption: 0.454533333,
		stage: "02:49AM"
	},
	{
		production: -0.002516667,
		consumption: 0.436033333,
		stage: "02:50AM"
	},
	{
		production: -0.0026,
		consumption: 0.3976,
		stage: "02:51AM"
	},
	{
		production: -0.002566667,
		consumption: 0.408633333,
		stage: "02:52AM"
	},
	{
		production: -0.0026,
		consumption: 0.405416667,
		stage: "02:53AM"
	},
	{
		production: -0.002566667,
		consumption: 0.405,
		stage: "02:54AM"
	},
	{
		production: -0.00255,
		consumption: 0.4058,
		stage: "02:55AM"
	},
	{
		production: -0.002566667,
		consumption: 0.395733333,
		stage: "02:56AM"
	},
	{
		production: -0.002533333,
		consumption: 0.405633333,
		stage: "02:57AM"
	},
	{
		production: -0.002466667,
		consumption: 0.4431,
		stage: "02:58AM"
	},
	{
		production: -0.002483333,
		consumption: 0.440683333,
		stage: "02:59AM"
	},
	{
		production: -0.00245,
		consumption: 0.4493,
		stage: "03:00AM"
	},
	{
		production: -0.0025,
		consumption: 0.448866667,
		stage: "03:01AM"
	},
	{
		production: -0.002433333,
		consumption: 0.4615,
		stage: "03:02AM"
	},
	{
		production: -0.002033333,
		consumption: 0.63295,
		stage: "03:03AM"
	},
	{
		production: -0.002466667,
		consumption: 0.43425,
		stage: "03:04AM"
	},
	{
		production: -0.002566667,
		consumption: 0.372266667,
		stage: "03:05AM"
	},
	{
		production: -0.002133333,
		consumption: 0.539766667,
		stage: "03:06AM"
	},
	{
		production: -0.001966667,
		consumption: 0.6051,
		stage: "03:07AM"
	},
	{
		production: -0.001733333,
		consumption: 1.045116667,
		stage: "03:08AM"
	},
	{
		production: -0.00185,
		consumption: 1.0849,
		stage: "03:09AM"
	},
	{
		production: -0.002216667,
		consumption: 0.892466667,
		stage: "03:10AM"
	},
	{
		production: -0.002316667,
		consumption: 0.838683333,
		stage: "03:11AM"
	},
	{
		production: -0.002366667,
		consumption: 0.817116667,
		stage: "03:12AM"
	},
	{
		production: -0.002383333,
		consumption: 0.810883333,
		stage: "03:13AM"
	},
	{
		production: -0.002383333,
		consumption: 0.804583333,
		stage: "03:14AM"
	},
	{
		production: -0.0018,
		consumption: 1.079866667,
		stage: "03:15AM"
	},
	{
		production: -0.000666667,
		consumption: 1.7861,
		stage: "03:16AM"
	},
	{
		production: -0.000516667,
		consumption: 1.888816667,
		stage: "03:17AM"
	},
	{
		production: -0.000533333,
		consumption: 1.871,
		stage: "03:18AM"
	},
	{
		production: -0.000566667,
		consumption: 1.88245,
		stage: "03:19AM"
	},
	{
		production: -0.0015,
		consumption: 1.242933333,
		stage: "03:20AM"
	},
	{
		production: -0.000683333,
		consumption: 1.784683333,
		stage: "03:21AM"
	},
	{
		production: -0.000466667,
		consumption: 1.898383333,
		stage: "03:22AM"
	},
	{
		production: -0.000516667,
		consumption: 1.885533333,
		stage: "03:23AM"
	},
	{
		production: -0.0005,
		consumption: 1.889116667,
		stage: "03:24AM"
	},
	{
		production: -0.001466667,
		consumption: 1.261033333,
		stage: "03:25AM"
	},
	{
		production: -0.00075,
		consumption: 1.73295,
		stage: "03:26AM"
	},
	{
		production: -0.0006,
		consumption: 1.841233333,
		stage: "03:27AM"
	},
	{
		production: -0.000566667,
		consumption: 1.859883333,
		stage: "03:28AM"
	},
	{
		production: -0.000633333,
		consumption: 1.8355,
		stage: "03:29AM"
	},
	{
		production: -0.001483333,
		consumption: 1.267216667,
		stage: "03:30AM"
	},
	{
		production: -0.0008,
		consumption: 1.719533333,
		stage: "03:31AM"
	},
	{
		production: -0.000583333,
		consumption: 1.847833333,
		stage: "03:32AM"
	},
	{
		production: -0.0005,
		consumption: 1.877233333,
		stage: "03:33AM"
	},
	{
		production: -0.000533333,
		consumption: 1.8797,
		stage: "03:34AM"
	},
	{
		production: -0.001416667,
		consumption: 1.30085,
		stage: "03:35AM"
	},
	{
		production: -0.000733333,
		consumption: 1.746433333,
		stage: "03:36AM"
	},
	{
		production: -0.0006,
		consumption: 1.835066667,
		stage: "03:37AM"
	},
	{
		production: -0.000566667,
		consumption: 1.8419,
		stage: "03:38AM"
	},
	{
		production: -0.000583333,
		consumption: 1.841583333,
		stage: "03:39AM"
	},
	{
		production: -0.001466667,
		consumption: 1.269566667,
		stage: "03:40AM"
	},
	{
		production: -0.0008,
		consumption: 1.698416667,
		stage: "03:41AM"
	},
	{
		production: -0.000566667,
		consumption: 1.846866667,
		stage: "03:42AM"
	},
	{
		production: -0.000583333,
		consumption: 1.838433333,
		stage: "03:43AM"
	},
	{
		production: -0.000533333,
		consumption: 1.886233333,
		stage: "03:44AM"
	},
	{
		production: -0.001366667,
		consumption: 1.3349,
		stage: "03:45AM"
	},
	{
		production: -0.000733333,
		consumption: 1.781816667,
		stage: "03:46AM"
	},
	{
		production: -0.000483333,
		consumption: 1.968266667,
		stage: "03:47AM"
	},
	{
		production: -0.000566667,
		consumption: 1.91245,
		stage: "03:48AM"
	},
	{
		production: -0.00055,
		consumption: 1.921516667,
		stage: "03:49AM"
	},
	{
		production: -0.001416667,
		consumption: 1.374833333,
		stage: "03:50AM"
	},
	{
		production: -0.000866667,
		consumption: 1.693683333,
		stage: "03:51AM"
	},
	{
		production: -0.000583333,
		consumption: 1.852766667,
		stage: "03:52AM"
	},
	{
		production: -0.000583333,
		consumption: 1.849983333,
		stage: "03:53AM"
	},
	{
		production: -0.000683333,
		consumption: 1.7612,
		stage: "03:54AM"
	},
	{
		production: -0.00155,
		consumption: 1.2297,
		stage: "03:55AM"
	},
	{
		production: -0.000933333,
		consumption: 1.61665,
		stage: "03:56AM"
	},
	{
		production: -0.00065,
		consumption: 1.788783333,
		stage: "03:57AM"
	},
	{
		production: -0.000666667,
		consumption: 1.79885,
		stage: "03:58AM"
	},
	{
		production: -0.000683333,
		consumption: 1.780716667,
		stage: "03:59AM"
	},
	{
		production: -0.001566667,
		consumption: 1.226883333,
		stage: "04:00AM"
	},
	{
		production: -0.001033333,
		consumption: 1.56305,
		stage: "04:01AM"
	},
	{
		production: -0.00075,
		consumption: 1.74265,
		stage: "04:02AM"
	},
	{
		production: -0.00105,
		consumption: 1.53765,
		stage: "04:03AM"
	},
	{
		production: -0.001883333,
		consumption: 1.034033333,
		stage: "04:04AM"
	},
	{
		production: -0.001916667,
		consumption: 1.01745,
		stage: "04:05AM"
	},
	{
		production: -0.0019,
		consumption: 1.026183333,
		stage: "04:06AM"
	},
	{
		production: -0.001633333,
		consumption: 1.180733333,
		stage: "04:07AM"
	},
	{
		production: -0.00165,
		consumption: 1.176033333,
		stage: "04:08AM"
	},
	{
		production: -0.001633333,
		consumption: 1.181383333,
		stage: "04:09AM"
	},
	{
		production: -0.001633333,
		consumption: 1.16245,
		stage: "04:10AM"
	},
	{
		production: -0.0017,
		consumption: 1.114133333,
		stage: "04:11AM"
	},
	{
		production: -0.001683333,
		consumption: 1.120483333,
		stage: "04:12AM"
	},
	{
		production: -0.00175,
		consumption: 1.075883333,
		stage: "04:13AM"
	},
	{
		production: -0.001733333,
		consumption: 1.076883333,
		stage: "04:14AM"
	},
	{
		production: -0.001716667,
		consumption: 1.0792,
		stage: "04:15AM"
	},
	{
		production: -0.001783333,
		consumption: 1.0661,
		stage: "04:16AM"
	},
	{
		production: -0.001716667,
		consumption: 1.076,
		stage: "04:17AM"
	},
	{
		production: -0.001716667,
		consumption: 1.074533333,
		stage: "04:18AM"
	},
	{
		production: -0.000683333,
		consumption: 1.753683333,
		stage: "04:19AM"
	},
	{
		production: -0.000833333,
		consumption: 1.672766667,
		stage: "04:20AM"
	},
	{
		production: -0.000883333,
		consumption: 1.63755,
		stage: "04:21AM"
	},
	{
		production: -0.001633333,
		consumption: 1.132233333,
		stage: "04:22AM"
	},
	{
		production: -0.001666667,
		consumption: 1.11475,
		stage: "04:23AM"
	},
	{
		production: -0.0011,
		consumption: 1.468783333,
		stage: "04:24AM"
	},
	{
		production: -0.000866667,
		consumption: 1.597316667,
		stage: "04:25AM"
	},
	{
		production: -0.00145,
		consumption: 1.224383333,
		stage: "04:26AM"
	},
	{
		production: -0.001683333,
		consumption: 1.0634,
		stage: "04:27AM"
	},
	{
		production: -0.001283333,
		consumption: 1.327816667,
		stage: "04:28AM"
	},
	{
		production: -0.000833333,
		consumption: 1.602083333,
		stage: "04:29AM"
	},
	{
		production: -0.001266667,
		consumption: 1.342983333,
		stage: "04:30AM"
	},
	{
		production: -0.001666667,
		consumption: 1.066116667,
		stage: "04:31AM"
	},
	{
		production: -0.001466667,
		consumption: 1.208466667,
		stage: "04:32AM"
	},
	{
		production: -0.000833333,
		consumption: 1.629683333,
		stage: "04:33AM"
	},
	{
		production: -0.001016667,
		consumption: 1.521866667,
		stage: "04:34AM"
	},
	{
		production: -0.001666667,
		consumption: 1.095283333,
		stage: "04:35AM"
	},
	{
		production: -0.00185,
		consumption: 0.980416667,
		stage: "04:36AM"
	},
	{
		production: -0.001083333,
		consumption: 1.4595,
		stage: "04:37AM"
	},
	{
		production: -0.0011,
		consumption: 1.45705,
		stage: "04:38AM"
	},
	{
		production: -0.001833333,
		consumption: 0.989116667,
		stage: "04:39AM"
	},
	{
		production: -0.0019,
		consumption: 0.946566667,
		stage: "04:40AM"
	},
	{
		production: -0.001316667,
		consumption: 1.311766667,
		stage: "04:41AM"
	},
	{
		production: -0.001066667,
		consumption: 1.466616667,
		stage: "04:42AM"
	},
	{
		production: -0.00165,
		consumption: 1.1113,
		stage: "04:43AM"
	},
	{
		production: -0.001883333,
		consumption: 0.968033333,
		stage: "04:44AM"
	},
	{
		production: -0.0015,
		consumption: 1.215916667,
		stage: "04:45AM"
	},
	{
		production: -0.000983333,
		consumption: 1.524816667,
		stage: "04:46AM"
	},
	{
		production: -0.001383333,
		consumption: 1.2921,
		stage: "04:47AM"
	},
	{
		production: -0.001983333,
		consumption: 0.7517,
		stage: "04:48AM"
	},
	{
		production: -0.0021,
		consumption: 0.581,
		stage: "04:49AM"
	},
	{
		production: -0.001366667,
		consumption: 1.018933333,
		stage: "04:50AM"
	},
	{
		production: -0.001783333,
		consumption: 0.839083333,
		stage: "04:51AM"
	},
	{
		production: -0.002633333,
		consumption: 0.391983333,
		stage: "04:52AM"
	},
	{
		production: -0.0026,
		consumption: 0.400983333,
		stage: "04:53AM"
	},
	{
		production: -0.001666667,
		consumption: 0.902983333,
		stage: "04:54AM"
	},
	{
		production: -0.001666667,
		consumption: 0.874216667,
		stage: "04:55AM"
	},
	{
		production: -0.002366667,
		consumption: 0.465983333,
		stage: "04:56AM"
	},
	{
		production: -0.00235,
		consumption: 0.480333333,
		stage: "04:57AM"
	},
	{
		production: -0.00175,
		consumption: 0.8258,
		stage: "04:58AM"
	},
	{
		production: -0.001383333,
		consumption: 1.013016667,
		stage: "04:59AM"
	},
	{
		production: -0.002033333,
		consumption: 0.665083333,
		stage: "05:00AM"
	},
	{
		production: -0.002433333,
		consumption: 0.432716667,
		stage: "05:01AM"
	},
	{
		production: -0.002083333,
		consumption: 0.63,
		stage: "05:02AM"
	},
	{
		production: -0.0015,
		consumption: 0.962816667,
		stage: "05:03AM"
	},
	{
		production: -0.00185,
		consumption: 0.772583333,
		stage: "05:04AM"
	},
	{
		production: -0.0025,
		consumption: 0.427316667,
		stage: "05:05AM"
	},
	{
		production: -0.002433333,
		consumption: 0.41715,
		stage: "05:06AM"
	},
	{
		production: -0.002466667,
		consumption: 0.42555,
		stage: "05:07AM"
	},
	{
		production: -0.002433333,
		consumption: 0.42475,
		stage: "05:08AM"
	},
	{
		production: -0.00235,
		consumption: 0.471533333,
		stage: "05:09AM"
	},
	{
		production: -0.002333333,
		consumption: 0.4715,
		stage: "05:10AM"
	},
	{
		production: -0.00235,
		consumption: 0.470466667,
		stage: "05:11AM"
	},
	{
		production: -0.002366667,
		consumption: 0.460866667,
		stage: "05:12AM"
	},
	{
		production: -0.002433333,
		consumption: 0.424316667,
		stage: "05:13AM"
	},
	{
		production: -0.002383333,
		consumption: 0.4194,
		stage: "05:14AM"
	},
	{
		production: -0.002383333,
		consumption: 0.418983333,
		stage: "05:15AM"
	},
	{
		production: -0.002416667,
		consumption: 0.409383333,
		stage: "05:16AM"
	},
	{
		production: -0.002433333,
		consumption: 0.418683333,
		stage: "05:17AM"
	},
	{
		production: -0.0025,
		consumption: 0.418233333,
		stage: "05:18AM"
	},
	{
		production: -0.00235,
		consumption: 0.4174,
		stage: "05:19AM"
	},
	{
		production: -0.002383333,
		consumption: 0.408983333,
		stage: "05:20AM"
	},
	{
		production: -0.002283333,
		consumption: 0.46225,
		stage: "05:21AM"
	},
	{
		production: -0.002266667,
		consumption: 0.46255,
		stage: "05:22AM"
	},
	{
		production: -0.002316667,
		consumption: 0.4529,
		stage: "05:23AM"
	},
	{
		production: -0.0024,
		consumption: 0.398766667,
		stage: "05:24AM"
	},
	{
		production: -0.0026,
		consumption: 0.306383333,
		stage: "05:25AM"
	},
	{
		production: -0.002583333,
		consumption: 0.304516667,
		stage: "05:26AM"
	},
	{
		production: -0.0026,
		consumption: 0.295133333,
		stage: "05:27AM"
	},
	{
		production: -0.002616667,
		consumption: 0.313333333,
		stage: "05:28AM"
	},
	{
		production: -0.002616667,
		consumption: 0.29495,
		stage: "05:29AM"
	},
	{
		production: -0.002633333,
		consumption: 0.304166667,
		stage: "05:30AM"
	},
	{
		production: -0.002616667,
		consumption: 0.295033333,
		stage: "05:31AM"
	},
	{
		production: -0.002533333,
		consumption: 0.338366667,
		stage: "05:32AM"
	},
	{
		production: -0.00255,
		consumption: 0.3522,
		stage: "05:33AM"
	},
	{
		production: -0.00255,
		consumption: 0.336733333,
		stage: "05:34AM"
	},
	{
		production: -0.002516667,
		consumption: 0.3451,
		stage: "05:35AM"
	},
	{
		production: -0.002566667,
		consumption: 0.32285,
		stage: "05:36AM"
	},
	{
		production: -0.002616667,
		consumption: 0.29845,
		stage: "05:37AM"
	},
	{
		production: -0.002633333,
		consumption: 0.290583333,
		stage: "05:38AM"
	},
	{
		production: -0.002633333,
		consumption: 0.300316667,
		stage: "05:39AM"
	},
	{
		production: -0.0026,
		consumption: 0.30185,
		stage: "05:40AM"
	},
	{
		production: -0.002616667,
		consumption: 0.30315,
		stage: "05:41AM"
	},
	{
		production: -0.00265,
		consumption: 0.294466667,
		stage: "05:42AM"
	},
	{
		production: -0.002616667,
		consumption: 0.303783333,
		stage: "05:43AM"
	},
	{
		production: -0.0026,
		consumption: 0.32755,
		stage: "05:44AM"
	},
	{
		production: -0.00255,
		consumption: 0.3422,
		stage: "05:45AM"
	},
	{
		production: -0.002533333,
		consumption: 0.349666667,
		stage: "05:46AM"
	},
	{
		production: -0.002566667,
		consumption: 0.347183333,
		stage: "05:47AM"
	},
	{
		production: -0.002566667,
		consumption: 0.343266667,
		stage: "05:48AM"
	},
	{
		production: -0.002583333,
		consumption: 0.3202,
		stage: "05:49AM"
	},
	{
		production: -0.002633333,
		consumption: 0.301333333,
		stage: "05:50AM"
	},
	{
		production: -0.002633333,
		consumption: 0.310183333,
		stage: "05:51AM"
	},
	{
		production: -0.002616667,
		consumption: 0.310466667,
		stage: "05:52AM"
	},
	{
		production: -0.002483333,
		consumption: 0.379983333,
		stage: "05:53AM"
	},
	{
		production: -0.002416667,
		consumption: 0.42835,
		stage: "05:54AM"
	},
	{
		production: -0.002433333,
		consumption: 0.421133333,
		stage: "05:55AM"
	},
	{
		production: -0.00245,
		consumption: 0.410366667,
		stage: "05:56AM"
	},
	{
		production: -0.002366667,
		consumption: 0.44125,
		stage: "05:57AM"
	},
	{
		production: -0.002333333,
		consumption: 0.464033333,
		stage: "05:58AM"
	},
	{
		production: -0.002333333,
		consumption: 0.454533333,
		stage: "05:59AM"
	},
	{
		production: -0.00235,
		consumption: 0.463333333,
		stage: "06:00AM"
	},
	{
		production: -0.002366667,
		consumption: 0.43875,
		stage: "06:01AM"
	},
	{
		production: -0.002433333,
		consumption: 0.41515,
		stage: "06:02AM"
	},
	{
		production: -0.002433333,
		consumption: 0.406333333,
		stage: "06:03AM"
	},
	{
		production: -0.002416667,
		consumption: 0.4167,
		stage: "06:04AM"
	},
	{
		production: -0.0024,
		consumption: 0.414933333,
		stage: "06:05AM"
	},
	{
		production: -0.002366667,
		consumption: 0.4156,
		stage: "06:06AM"
	},
	{
		production: -0.002416667,
		consumption: 0.40575,
		stage: "06:07AM"
	},
	{
		production: -0.0024,
		consumption: 0.41495,
		stage: "06:08AM"
	},
	{
		production: -0.002583333,
		consumption: 0.435183333,
		stage: "06:09AM"
	},
	{
		production: -0.002716667,
		consumption: 0.4517,
		stage: "06:10AM"
	},
	{
		production: -0.0027,
		consumption: 0.460533333,
		stage: "06:11AM"
	},
	{
		production: -0.0027,
		consumption: 0.454983333,
		stage: "06:12AM"
	},
	{
		production: -0.002683333,
		consumption: 0.44475,
		stage: "06:13AM"
	},
	{
		production: -0.002766667,
		consumption: 0.416866667,
		stage: "06:14AM"
	},
	{
		production: -0.002783333,
		consumption: 0.399333333,
		stage: "06:15AM"
	},
	{
		production: -0.002816667,
		consumption: 0.408116667,
		stage: "06:16AM"
	},
	{
		production: -0.002816667,
		consumption: 0.407883333,
		stage: "06:17AM"
	},
	{
		production: -0.002816667,
		consumption: 0.399033333,
		stage: "06:18AM"
	},
	{
		production: -0.0028,
		consumption: 0.405033333,
		stage: "06:19AM"
	},
	{
		production: -0.002833333,
		consumption: 0.3564,
		stage: "06:20AM"
	},
	{
		production: -0.003033333,
		consumption: 0.2867,
		stage: "06:21AM"
	},
	{
		production: -0.002966667,
		consumption: 0.318266667,
		stage: "06:22AM"
	},
	{
		production: -0.002916667,
		consumption: 0.341016667,
		stage: "06:23AM"
	},
	{
		production: -0.0029,
		consumption: 0.332783333,
		stage: "06:24AM"
	},
	{
		production: -0.002883333,
		consumption: 0.34125,
		stage: "06:25AM"
	},
	{
		production: -0.002933333,
		consumption: 0.318316667,
		stage: "06:26AM"
	},
	{
		production: -0.00295,
		consumption: 0.295066667,
		stage: "06:27AM"
	},
	{
		production: -0.002983333,
		consumption: 0.295916667,
		stage: "06:28AM"
	},
	{
		production: -0.002966667,
		consumption: 0.2875,
		stage: "06:29AM"
	},
	{
		production: -0.00295,
		consumption: 0.29495,
		stage: "06:30AM"
	},
	{
		production: -0.002966667,
		consumption: 0.29555,
		stage: "06:31AM"
	},
	{
		production: -0.002966667,
		consumption: 0.287116667,
		stage: "06:32AM"
	},
	{
		production: -0.00295,
		consumption: 0.295283333,
		stage: "06:33AM"
	},
	{
		production: -0.002866667,
		consumption: 0.3414,
		stage: "06:34AM"
	},
	{
		production: -0.002933333,
		consumption: 0.332533333,
		stage: "06:35AM"
	},
	{
		production: -0.002883333,
		consumption: 0.34085,
		stage: "06:36AM"
	},
	{
		production: -0.002866667,
		consumption: 0.341316667,
		stage: "06:37AM"
	},
	{
		production: -0.003,
		consumption: 0.29465,
		stage: "06:38AM"
	},
	{
		production: -0.002966667,
		consumption: 0.295466667,
		stage: "06:39AM"
	},
	{
		production: -0.002983333,
		consumption: 0.287966667,
		stage: "06:40AM"
	},
	{
		production: -0.002966667,
		consumption: 0.29425,
		stage: "06:41AM"
	},
	{
		production: -0.002966667,
		consumption: 0.2953,
		stage: "06:42AM"
	},
	{
		production: -0.002966667,
		consumption: 0.2873,
		stage: "06:43AM"
	},
	{
		production: -0.002966667,
		consumption: 0.299633333,
		stage: "06:44AM"
	},
	{
		production: -0.002883333,
		consumption: 0.330333333,
		stage: "06:45AM"
	},
	{
		production: -0.002866667,
		consumption: 0.343333333,
		stage: "06:46AM"
	},
	{
		production: -0.0029,
		consumption: 0.349766667,
		stage: "06:47AM"
	},
	{
		production: -0.002866667,
		consumption: 0.359083333,
		stage: "06:48AM"
	},
	{
		production: -0.002816667,
		consumption: 0.394533333,
		stage: "06:49AM"
	},
	{
		production: -0.00275,
		consumption: 0.41695,
		stage: "06:50AM"
	},
	{
		production: -0.002733333,
		consumption: 0.414633333,
		stage: "06:51AM"
	},
	{
		production: -0.002766667,
		consumption: 0.414066667,
		stage: "06:52AM"
	},
	{
		production: -0.002766667,
		consumption: 0.405833333,
		stage: "06:53AM"
	},
	{
		production: -0.002766667,
		consumption: 0.419866667,
		stage: "06:54AM"
	},
	{
		production: -0.002816667,
		consumption: 0.40225,
		stage: "06:55AM"
	},
	{
		production: -0.0028,
		consumption: 0.41145,
		stage: "06:56AM"
	},
	{
		production: -0.002716667,
		consumption: 0.428466667,
		stage: "06:57AM"
	},
	{
		production: -0.0027,
		consumption: 0.4553,
		stage: "06:58AM"
	},
	{
		production: -0.002666667,
		consumption: 0.455883333,
		stage: "06:59AM"
	},
	{
		production: -0.0027,
		consumption: 0.448333333,
		stage: "07:00AM"
	},
	{
		production: -0.002666667,
		consumption: 0.453633333,
		stage: "07:01AM"
	},
	{
		production: -0.002733333,
		consumption: 0.408716667,
		stage: "07:02AM"
	},
	{
		production: -0.00275,
		consumption: 0.411383333,
		stage: "07:03AM"
	},
	{
		production: -0.002783333,
		consumption: 0.408583333,
		stage: "07:04AM"
	},
	{
		production: -0.00275,
		consumption: 0.409983333,
		stage: "07:05AM"
	},
	{
		production: -0.002783333,
		consumption: 0.402416667,
		stage: "07:06AM"
	},
	{
		production: -0.00275,
		consumption: 0.407616667,
		stage: "07:07AM"
	},
	{
		production: -0.00275,
		consumption: 0.4091,
		stage: "07:08AM"
	},
	{
		production: -0.002783333,
		consumption: 0.4018,
		stage: "07:09AM"
	},
	{
		production: -0.0027,
		consumption: 0.4338,
		stage: "07:10AM"
	},
	{
		production: -0.002666667,
		consumption: 0.454016667,
		stage: "07:11AM"
	},
	{
		production: -0.002683333,
		consumption: 0.447033333,
		stage: "07:12AM"
	},
	{
		production: -0.002666667,
		consumption: 0.45255,
		stage: "07:13AM"
	},
	{
		production: -0.0027,
		consumption: 0.430833333,
		stage: "07:14AM"
	},
	{
		production: -0.002766667,
		consumption: 0.4069,
		stage: "07:15AM"
	},
	{
		production: -0.00275,
		consumption: 0.407933333,
		stage: "07:16AM"
	},
	{
		production: -0.002866667,
		consumption: 0.366016667,
		stage: "07:17AM"
	},
	{
		production: -0.003,
		consumption: 0.2954,
		stage: "07:18AM"
	},
	{
		production: -0.003,
		consumption: 0.2974,
		stage: "07:19AM"
	},
	{
		production: -0.003009259,
		consumption: 0.29362963,
		stage: "07:20AM"
	},
	{
		production: -0.002933333,
		consumption: 0.338183333,
		stage: "07:21AM"
	},
	{
		production: -0.002916667,
		consumption: 0.335183333,
		stage: "07:22AM"
	},
	{
		production: -0.0029,
		consumption: 0.344183333,
		stage: "07:23AM"
	},
	{
		production: -0.002933333,
		consumption: 0.343716667,
		stage: "07:24AM"
	},
	{
		production: -0.003,
		consumption: 0.303733333,
		stage: "07:25AM"
	},
	{
		production: -0.003016667,
		consumption: 0.297533333,
		stage: "07:26AM"
	},
	{
		production: -0.003016667,
		consumption: 0.289133333,
		stage: "07:27AM"
	},
	{
		production: -0.003016667,
		consumption: 0.29845,
		stage: "07:28AM"
	},
	{
		production: -0.003016667,
		consumption: 0.297966667,
		stage: "07:29AM"
	}
];
let chartData;
const getColorProduction = (range, max) => {
	if (range < 0) range = 0;
	let colorRange = (max / 60);
	let actRange = Math.ceil(range / colorRange);
	return `hsla(202, 83%, ${90 - actRange}%, 1)`;
};

const getColorConsumption = (range, max) => {
	if (range < 0) range = 0;
	let colorRange = (max / 60);
	let actRange = Math.ceil(range / colorRange);
	return `hsla(41, 84%, ${90 - actRange}%, 1)`;
};

class TwoLayerDonutChart extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isHovered: false,
			data: null
		};
	}
	interpolateOutter = d3.interpolateRgb("#36384b", "#4992ab");
	dataOutter = [1];
	path(radius, interpolate, slice, sliceColor, changeChartData) {
		const maxLimit = 10;
		if (slice.data) {
			sliceColor = getColorConsumption(slice.data.production, maxLimit);
		}
		const outerRadius = this.state.isHovered ? radius * 0.7 : radius;
		const innerRadius = radius * 0.5;
		let sliceColorOutter = "white";
		if (slice.data) {
			sliceColorOutter = getColorProduction(slice.data.consumption, maxLimit);
		}
		const arc = d3
			.arc()
			.innerRadius(innerRadius)
			.outerRadius(outerRadius)
			.padAngle(0.0)
			.cornerRadius(2);

		let outterPie = d3
			.pie()
			.startAngle(slice.startAngle)
			.endAngle(slice.endAngle);

		const arc2 = d3
			.arc()
			.innerRadius(outerRadius * 1.15) //used for setting the gap between layers
			.outerRadius(outerRadius * 1.39)
			.padAngle(0.0001)
			.cornerRadius(1)
			;

		return (
			<g>
				<path
					d={arc(slice)}
					fill={sliceColor}
					data={JSON.stringify(slice.data)}
				>
					<title>
						{slice.data.stage},production : {slice.data.production}
					</title>
				</path>

				{outterPie(this.dataOutter).map((outterSlice, index) => {
					return (
						<path
							d={arc2(outterSlice)}
							fill={sliceColorOutter}
							data={JSON.stringify(slice.data)}
						>
							<title>
								{slice.data.stage},consumption : {slice.data.consumption}
							</title>
						</path>
					);
				})}
				{this.state.isHovered && (
					<circle r={innerRadius * 0.95} fill={"white"} />
				)}
			</g>
		);
	}
	slice = (pie, changeChartData) => {
		let interpolateInner = d3.interpolateRgb("#eaaf79", "#bc3358");
		let timeDiff = parseInt(dataInner[1].stage.split(':')[1]) - parseInt(dataInner[0].stage.split(':')[1]);
		let numberOfSlice = 1440;
		if(timeDiff < 15) 
			numberOfSlice = 1440;
		else 
			numberOfSlice = 96;
		
		return pie.map((slice, index) => {
			let sliceColor = "white"; //color of inner layer
			const partitionRadian = 6.2831 / numberOfSlice;
			slice.startAngle = 0 + index * partitionRadian; // used for dividing the equal partition
			slice.endAngle = partitionRadian + index * partitionRadian;
			return this.path(
				130,
				interpolateInner,
				slice,
				sliceColor,	
				changeChartData
			);
		});
	};
	render() {
		const height = 400;
		const width = 400;
		let pie = d3.pie()(dataInner);
		return (
			<div className="wrapperChart">
				<div className="chartCenterData positionAbsolute">
					{this.props.data
						? Math.round(
								(this.props.data.production / this.props.data.consumption) * 100
						  ) + "% powered by sun"
						: ""}
				</div>
				<div className="timeContainer">
					<span className="pM12 positionAbsolute clockTime">12 P.M.</span>
					<span className="pM3 positionAbsolute clockTime">3 P.M.</span>
					<span className="pM6 positionAbsolute clockTime">6 P.M.</span>
					<span className="pM9 positionAbsolute clockTime">9 P.M.</span>
					<span className="aM12 positionAbsolute clockTime">12 A.M.</span>
					<span className="aM3 positionAbsolute clockTime">3 A.M.</span>
					<span className="aM6 positionAbsolute clockTime">6 A.M.</span>
					<span className="aM9 positionAbsolute clockTime">9 A.M.</span>
				</div>
				<svg height={height} width={width} className="chartSvg">
					<g transform={`translate(${width / 2},${height / 2})`}>
						{this.slice(pie, this.props.changeChartData)}
					</g>
				</svg>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		data: state.chartData.data
	};
};

const mapDispachToProps = dispatch => {
	return {
		changeChartData: () =>
			dispatch({ type: "CHANGE_CHART_DATA", chartDatas: chartData })
	};
};

export default connect(
	mapStateToProps,
	mapDispachToProps
)(TwoLayerDonutChart);
