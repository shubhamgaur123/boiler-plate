import React from 'react';
import { connect } from 'react-redux'

class TestReducerClass extends React.Component {
    componentDidMount() {
        // console.log('props::', this.props.changeClass);
        this.props.changeClass();
    }
    render() {
        return (
            <>
                {'{'}
                Class : {this.props.class}
                {'}'}
            </>
        )
    }
}

const mapStateToProps = state => {
    // console.log('class state :: ', state);
    return {
        class: state.reducerClass.class
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeClass: () => dispatch({ type: 'CHANGE_CLASS' })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestReducerClass);

