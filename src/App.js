import React, { Component,Suspense,lazy } from "react";
import { connect } from "react-redux";
//import { Progress } from "antd";
import Loadable from 'react-loadable';


import "./App.css";

//import D3 from './components/d3';
import TestReducer from './components/TestReducer'
import TestReducerClass from './components/TestReducerClass'
//import TwoLayerDonutChart from './components/TwoLayerDonutChart'
import Loading from './components/Loading'

const TwoLayerDonutChart = React.lazy(() => import('./components/TwoLayerDonutChart'));
// const TwoLayerDonutChart = Loadable({
//   loader: () => import('./components/TwoLayerDonutChart'),
//   loading: Loading,
// });

class App extends Component {

	render() {
		let { age, onAgeUp, onAgeDown } = this.props;

		return (
			<>
				<div className="App">
					<div>
						Age is: <span>{age}</span>
					</div>
					<button onClick={onAgeUp}>Age UP</button>
					<button onClick={onAgeDown}>Age DOWN</button>
				</div>
				<TestReducer />
				<hr>
				</hr>
				<Suspense fallback={<div>this is loading ...</div>} >
				<TwoLayerDonutChart />
				</Suspense>
				{/* <Progress
					type="circle"
					percent={75}
					format={percent => `${percent} % Energy Consumed`}
					width="150px"
				/> */}

			</>
		);
	}
}

const mapStateToProps = state => {
	// console.log('state ::', state);
	return {
		age: state.reducerAge.age
	};
};

const mapDispachToProps = dispach => {
	return {
		onAgeUp: () => dispach({ type: "AGE_UP" }),
		onAgeDown: () => dispach({ type: "AGE_DOWN" })
	};
};

export default connect(
	mapStateToProps,
	mapDispachToProps
)(App);
