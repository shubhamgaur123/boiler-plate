import { takeEvery, takeLatest, put, delay } from "redux-saga/effects";

function* ageUpAsync() {
	yield delay(1000);
	yield put({ type: "AGE_UP_ASYNC" });
}
function* changeNameAsync() {
	yield delay(10000);
	yield put({ type: 'CHANGE_NAME_ASYNC' })
}
export function* watchAgeUp() {
	yield takeEvery("AGE_UP", ageUpAsync);
	yield takeLatest('CHANGE_NAME', changeNameAsync);
}
