import { combineReducers } from "redux";

const initialState = {
	age: 21,
	name: 'Shubham',
};

const reducerAge = (state = initialState, action) => {
	const newState = { ...state };

	switch (action.type) {
		case "AGE_UP_ASYNC":
			newState.age++;
			break;
		case "AGE_DOWN":
			newState.age--;
			break;
		default:
			break;
	}
	return newState;
};

const reducerName = (state = initialState, action) => {
	let newState = { ...state };
	if (action.type === 'CHANGE_NAME_ASYNC') {
		newState.name = 'Shubham Gaur'
	}
	return newState;
}


const chartData = (state = { data: '' }, action) => {
	let newState = { ...state };
	newState.data = action.chartDatas;
	return newState;
}

const reducer = combineReducers(
	{
		reducerAge,
		reducerName,
		chartData
	}
)

export default reducer;
